class sssdad::install {
  $packages = lookup('sssdad::packages', Array[String], 'first', undef)
  Package { $packages:
    ensure => present
  }
}

