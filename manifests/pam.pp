class sssdad::pam {
  Exec { path => [ '/bin/', '/sbin/' , '/usr/bin/', '/usr/sbin/' ] }
  if $facts[osfamily] == 'Debian' {
    Exec {"pam_sss":
      command     => "pam-auth-update --profile sss",
      require     => Package["libpam-sss"],
    }
    Exec {"pam_mkhomedir":
      command     => "pam-auth-update --profile mkhomedir",
      require     => Package["libpam-sss"],
    }
    file_line { "pam_mkhomedir":
        path      => "/etc/pam.d/common-session",
        ensure    => "present",
        match     => "^sessuin*pam_mkhomedir.so*",
        line      => "session    required    pam_mkhomedir.so skel=/etc/skel/ umask=0022",
        append_on_no_match => true
    }

  } elsif $facts[osfamily] == 'RedHat' {
    Exec { "authconfig":
      command     => "authconfig --enablesssd --enablemkhomedir --update",
      require     => Package["sssd-client"],
    }
  }
}
