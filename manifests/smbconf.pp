class sssdad::smbconf(String $domain, String $workgroup) {

  $smbconf = lookup('sssdad::smbconf', String, 'first', undef)

  ini_setting { 'workgroup':
    ensure  => present,
    path    => $smbconf,
    section => 'global',
    setting => 'workgroup',
    value   => upcase($workgroup),
    key_val_separator => ' = ',
    indent_char    => " ",
    indent_width   => 2,
  }
  ini_setting { 'realm':
    ensure  => present,
    path    => $smbconf,
    section => 'global',
    setting => 'realm',
    value   => upcase($domain),
    key_val_separator => '=',
  }
  ini_setting { 'client signing':
    ensure  => present,
    path    => $smbconf,
    section => 'global',
    setting => 'client signing',
    value   => 'yes',
    key_val_separator => '=',
  }
  ini_setting { 'client use spnego':
    ensure  => present,
    path    => $smbconf,
    section => 'global',
    setting => 'client use spnego',
    value   => 'yes',
    key_val_separator => '=',
  }
  ini_setting { 'kerberos method':
    ensure  => present,
    path    => $smbconf,
    section => 'global',
    setting => 'kerberos method',
    value   => 'secrets and keytab',
    key_val_separator => '=',
  }
  ini_setting { 'security':
    ensure  => present,
    path    => $smbconf,
    section => 'global',
    setting => 'security',
    value   => 'ads',
    key_val_separator => '=',
  }

}

