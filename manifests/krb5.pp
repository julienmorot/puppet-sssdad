class sssdad::krb5(String $domain) {

  File { "krb5.conf":
    path    => "/etc/krb5.conf",
    ensure  => file,
    mode    => "644",
    owner   => "root",
    group   => "root",
    content => template("${module_name}/krb5.conf.erb"),
  }
}


