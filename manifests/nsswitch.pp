class sssdad::nsswitch {
  $nsswitchconf = lookup('sssdad::nsswitchconf', String, 'first', '/etc/nsswitch.conf')

  file_line { 'nss_passwd':
    ensure => present,
    path => $nsswitchconf,
    match => '^passwd',
    line => 'passwd:         compat systemd sss',
    append_on_no_match => true,
  }

  file_line { 'nss_group':
    ensure => present,
    path => $nsswitchconf,
    match => '^group',
    line => 'group:          compat systemd sss',
    append_on_no_match => true,
  }

  file_line { 'nss_shadow':
    ensure => present,
    path => $nsswitchconf,
    match => '^shadow',
    line => 'shadow:         compat sss',
    append_on_no_match => true,
  }
}

