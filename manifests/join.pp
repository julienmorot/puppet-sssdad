class sssdad::join {

  $join_username = lookup('sssdad::join_username', String, 'first', undef)
  $join_password = lookup('sssdad::join_password', String, 'first', undef)

  Exec { 'realm_join_with_password':
    path        => '/usr/bin:/usr/sbin:/bin',
    command     => "net ads join -k -U ${join_username}%${join_password}",
    unless      => "klist -k /etc/krb5.keytab",
  }

}

