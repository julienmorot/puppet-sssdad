class sssdad(String $domain, String $workgroup) {
    include sssdad::install
    class { sssdad::krb5: domain => $domain }
    class { sssdad::smbconf: domain => $domain, workgroup => $workgroup }
    include sssdad::join
    class { sssdad::sssd: domain => $domain }
    include sssdad::pam
    include sssdad::nsswitch
}
