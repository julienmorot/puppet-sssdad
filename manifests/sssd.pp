class sssdad::sssd(String $domain) {

  $sssdconf = lookup('sssdad::sssdconf', String, 'first', undef)

  $idmap = lookup('sssdad::idmap', String, 'first', 'False')
  $idmap_range_min = lookup('sssdad::idmap_range_min', Integer, 'first', 200000)
  $idmap_range_max = lookup('sssdad::idmap_range_max', Integer, 'first', 2000200000)

  $allow_users = lookup('sssdad::allow_users', Array[String], 'first', [])
  $allow_groups = lookup('sssdad::allow_groups', Array[String], 'first', []) 

  File { "sssd.conf":
    path    => $sssdconf,
    ensure  => file,
    mode    => "600",
    owner   => "root",
    group   => "root",
    content => template("${module_name}/sssd.conf.erb"),
    notify  => Service["sssd.service"]
  }

  Service { "sssd.service":
    ensure     => running,
    enable     => true,
    hasstatus  => true,
    hasrestart => true,
  }
}

