Facter.add("domain_sid") do
  setcode do
    if File.exist? '/etc/krb5.keytab'
      Facter::Core::Execution.execute('net getdomainsid | grep "SID for domain" | cut -d ":" -f 2')
    else
      nil
    end
  end
end

