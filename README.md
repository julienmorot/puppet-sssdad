# sssdad

#### Table of Contents

1. [Description](#description)
1. [Usage](#usage)
1. [TODO](#TODO)

## Description

This module manage the entire stack to enrol a Linux system to an AD DC :
- sssd
- krb5.conf
- smb.conf
- nsswitch.conf
- AD Computer membership
- PAM

Only tested against Ubuntu 18.04 until now.

## Usage

```
node 'adclient' {
    include sssdad
}
```

Then configure Hiera, for example :

/etc/puppetlabs/code/environments/production/data/common.yaml

```
---
sssdad::domain: "lab.morot.fr"
sssdad::workgroup: "LAB"
sssdad::join_username: "Administrator"
sssdad::join_password: "secret"
```

IDMapping :
If not specified, idmap is disabled. If you enable idmap without min/max 
```
sssdad::idmap: "True"
sssdad::idmap_range_min: 200000
sssdad::idmap_range_max: 2000200000
```

If specified, you can restrict authentication to selected users or groups :
```
sssdad::allow_users:
  - julien
  - guy
sssdad::allow_groups:
  - admins
  - operators
```

